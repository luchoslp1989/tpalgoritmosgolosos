package archivos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

 
import heuristicas.Distribuidor;

public class DistribuidorJson 
{
	private ArrayList<Distribuidor> distribuidores;
	 
	
	public DistribuidorJson()
	{
		distribuidores = new ArrayList<Distribuidor>();
	}
	
	public void addDistribuidor(Distribuidor d)
	{
		distribuidores.add(d);
	}
	
	public Distribuidor obtenerDistribuidor(int indice)
	{
		return distribuidores.get(indice);
	}
	
	public int tamano()
	{
		return distribuidores.size();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Distribuidor> obtenerDsitribuidor()
	{
		
		return (ArrayList<Distribuidor>) distribuidores.clone();
		
	}
	
	public String generarJsonpretty()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		
		return json;
	
	}
	
	public void guardarJson(String jsonParaGuardar, String archivoDestino)
	{
		try
		{
			FileWriter writer = new FileWriter(archivoDestino);
			writer.write(jsonParaGuardar);
			writer.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static DistribuidorJson leerJson(String archivo)
	{
		Gson gson = new Gson();
		DistribuidorJson ret = null;
		
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, DistribuidorJson.class);
		} catch (IOException e )
		{
			e.printStackTrace();
		}
		
		return ret;
	}
	
}
