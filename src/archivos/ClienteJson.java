package archivos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import heuristicas.Cliente;


public class ClienteJson 
{
	private ArrayList<Cliente> clientes;
	 
	
	public ClienteJson()
	{
		clientes = new ArrayList<Cliente>();
	}
	
	public void addCliente(Cliente c)
	{
		clientes.add(c);
	}
	
	public Cliente obtenerCliente(int indice)
	{
		return clientes.get(indice);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Cliente> obtenerClientes()
	{
		
		return (ArrayList<Cliente>) clientes.clone();
		
	}
	
	public int tamano()
	{
		return clientes.size();
	}
	
	public String generarJsonpretty()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		
		return json;
	
	}
	
	public void guardarJson(String jsonParaGuardar, String archivoDestino)
	{
		try
		{
			FileWriter writer = new FileWriter(archivoDestino);
			writer.write(jsonParaGuardar);
			writer.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static ClienteJson leerJson(String archivo)
	{
		Gson gson = new Gson();
		ClienteJson ret = null;
		
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, ClienteJson.class);
		} catch (IOException e )
		{
			e.printStackTrace();
		}
		
		return ret;
	}
	
}
