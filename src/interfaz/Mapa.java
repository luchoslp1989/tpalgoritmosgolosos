package interfaz;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import archivos.ClienteJson;
import archivos.DistribuidorJson;
import heuristicas.Cliente;
import heuristicas.Distribuidor;
import heuristicas.Instancia;
import heuristicas.Solucion;
import heuristicas.Solver;

import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Mapa {

	private JFrame frame;
	private static JMapViewer mapa;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private static ArrayList<Cliente> clientes;
	private static ArrayList<Distribuidor> distribuidores;
	private static ArrayList<Distribuidor> distribuidoresElegidos;
	private int cantidadDistribuidores;
	DecimalFormat formato;
	ArrayList<Coordinate> marcadores;

	private  ClienteJson json;
	private  DistribuidorJson jsonDistribuidor;
	private JFileChooser selectorArchivos;
	private Solver solver;
	private Solucion solucion;
	private Instancia instancia;
	private boolean CanceloArchivo=false;
	private JTextField costo;
	

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mapa window = new Mapa();
					window.frame.setVisible(true);
					 
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public Mapa() {
		initialize();
		
	}

	
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 753, 456);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		mapa = new JMapViewer();
		
		distribuidores = new ArrayList<Distribuidor>();
		distribuidoresElegidos = new ArrayList<Distribuidor>();
		clientes = new ArrayList<Cliente>();
		json= new ClienteJson();
		jsonDistribuidor= new DistribuidorJson();
		selectorArchivos = new JFileChooser();
		formato = new DecimalFormat("#.00");

	
		JPanel panelMapa = new JPanel();
		panelMapa.setBounds(6, 16, 458, 385);
		frame.getContentPane().add(panelMapa);
		panelMapa.add(mapa);
		Coordinate coordenada = new Coordinate(-34.525, -58.704);
		mapa.setDisplayPosition(coordenada, 15);
		
		textField_2 = new JTextField();
		textField_2.setBounds(590, 216, 125, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		
		JButton btnNewButton = new JButton("Obtener Distribuidores");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (!textField_2.getText().equals("") && esNumero()) {
					
					cantidadDistribuidores=Integer.valueOf(textField_2.getText());
					System.out.println(textField.getText());
					distribuidoresMasConvenientes();
					marcadorDistribuidores();
					actualizaMarcadores();
					costo.setText(String.valueOf(formato.format(solucion.costoTotal(instancia))));
					}
			
		else {
			JOptionPane.showMessageDialog(null, "Debe ingresar solo numeros!");
		}
				
				
				
		}
		});
		
		btnNewButton.setBounds(546, 247, 170, 32);
		frame.getContentPane().add(btnNewButton);
		
		textField = new JTextField();
		textField.setBounds(590, 21, 126, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		textField.setForeground(Color.BLACK);
		
		JButton exploracionClientes = new JButton("explorar");
		exploracionClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
			mostrarVentanaArchivos();
			
			if(!CanceloArchivo) {
			leeArchivoYagregaLosClientes();
			marcadorClientes();
			textField.setText("ARCHIVO CARGADO");
			
			}
		}
		});
		
		exploracionClientes.setBounds(625, 52, 89, 23);
		frame.getContentPane().add(exploracionClientes);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(590, 100, 125, 20);
		frame.getContentPane().add(textField_1);
		
		
		JButton exploracionDistribuidores = new JButton("explorar");
		exploracionDistribuidores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				mostrarVentanaArchivos();
				
				if(!CanceloArchivo) {
				leeArchivoYagregaLosDistribuidores();
				textField_1.setText("ARCHIVO CARGADO");
				
				}
			}
		});
		exploracionDistribuidores.setBounds(625, 132, 89, 23);
		frame.getContentPane().add(exploracionDistribuidores);
		
		
		String texto = "<html>Cantidad maxima <P>"+"<html>de Distribuidores :<P>";
		JLabel lblNewLabel = new JLabel(texto);
		lblNewLabel.setBounds(474, 182, 220, 80);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Lista de clientes :");
		lblNewLabel_1.setBounds(476, 24, 100, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		String texto2 = "<html>Posibles <P>"+"<html>distribuidores :<P>";
		JLabel lblNewLabel_2 = new JLabel(texto2);
		lblNewLabel_2.setBounds(490, 84, 250, 40);
		frame.getContentPane().add(lblNewLabel_2);
		
		costo = new JTextField();
		costo.setHorizontalAlignment(SwingConstants.CENTER);
		costo.setEditable(false);
		costo.setBounds(582, 319, 86, 20);
		frame.getContentPane().add(costo);
		costo.setColumns(10);
		
		JLabel lblNewLabel_1_1 = new JLabel("COSTO TOTAL");
		lblNewLabel_1_1.setBounds(590, 303, 125, 14);
		frame.getContentPane().add(lblNewLabel_1_1);
		
		
	}
	
	public boolean esNumero() {
		boolean TodasLetras=true;
		for (int i = 0; i < textField_2.getText().length(); i++) {
		char caracter = textField_2.getText().toUpperCase().charAt(i);
		int valorASCII = (int)caracter;
		
		if (valorASCII != 165 && (valorASCII > 64 && valorASCII < 91))
			 TodasLetras=TodasLetras && false	;}
		return TodasLetras ;
	}

	
	public void mostrarVentanaArchivos() {
		// muestra el cuadro de di�logo de archivos, para que el usuario pueda elegir el archivo a abrir
		selectorArchivos.setFileSelectionMode(JFileChooser.FILES_ONLY);
	 	
	 	//Indicar que solo deseamos localizar un tipo especifico de archivos
		FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos JSON", "JSON");
		selectorArchivos.setFileFilter(filtro);
		int entrada=selectorArchivos.showOpenDialog(null);
		
		//Si el usuario no elige ningun archivo y cancela.Cierra la ventana sin leer fichero inexistente
		if( entrada==JFileChooser.CANCEL_OPTION) {
			CanceloArchivo=true;
		}
	}
	
	
	
	
	public void leeArchivoYagregaLosClientes()
	{
		 String ruta=selectorArchivos.getSelectedFile().getPath();
		 ClienteJson Json=json.leerJson(ruta);
		 toArray(Json);
	}
	
	public void leeArchivoYagregaLosDistribuidores()
	{
		 String ruta=selectorArchivos.getSelectedFile().getPath();
		 DistribuidorJson Json=jsonDistribuidor.leerJson(ruta);
		 toArray(Json);
	}
	
	private static ArrayList<Cliente> toArray(ClienteJson json) {
		
		for (int i = 0; i < json.tamano(); i++) 
		{
			clientes.add(nuevoCliente(json, i));
		}
		return clientes;
	}
	
	
	private static Cliente nuevoCliente(ClienteJson json, int i) {
		return new Cliente(new Coordinate(json.obtenerCliente(i).getLatitud(), json.obtenerCliente(i).getLongitud()));
	}
	
	
	private static void marcadorClientes() {
		
		for(Cliente c : clientes) {
			Coordinate cordinate= new Coordinate(c.getLatitud(),c.getLongitud());
			MapMarker marcador= new MapMarkerDot("Cliente",cordinate);
			marcador.getStyle().setBackColor(Color.RED);
			mapa.addMapMarker(marcador);
			
			
		}
	}
	
	
	private static ArrayList<Distribuidor> toArray(DistribuidorJson json) {
		
		for (int i = 0; i < json.tamano(); i++) 
		{
			distribuidores.add(nuevoDistribuidor(json, i));
		}
		return distribuidores;
	}
	

	private static Distribuidor nuevoDistribuidor(DistribuidorJson json, int i) {
		return new Distribuidor(new Coordinate(json.obtenerDistribuidor(i).getLatitud(), json.obtenerDistribuidor(i).getLongitud()));
	}

	
    private static void marcadorDistribuidores() {
		
		
		for(Distribuidor c : distribuidoresElegidos) {
			
			Coordinate cordinate= new Coordinate(c.getLatitud(),c.getLongitud());
			MapMarker marcador= new MapMarkerDot("Distribuidor",cordinate);
			marcador.getStyle().setBackColor(Color.BLUE);
			mapa.addMapMarker(marcador);
		
		}
	}
    
   
    
   private void actualizaMarcadores() {
	  
	 mapa.removeAllMapMarkers();
	 marcadorDistribuidores();
	 marcadorClientes();
   }
	 
	   		
	 
   
    
    public void distribuidoresMasConvenientes() {
		instancia= new Instancia(cantidadDistribuidores);
	
		for( Distribuidor d: distribuidores) {
			instancia.agregarDistribuidor(d);
			for(Cliente c: clientes) { 
			 instancia.agregarCliente(c);
			 instancia.asignarDistancias();
			}
		 }
		
		solver = new Solver(instancia, (p,q) -> {return p.distanciaPromedio() > q.distanciaPromedio() ? 1
				: -1;
		});
		
		solucion = solver.resolver();
		distribuidoresElegidos= solucion.getDistribuidores();
		System.out.println(solucion.costoTotal(instancia));

	}
    
    
    
   
}
