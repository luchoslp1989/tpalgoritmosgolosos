package heuristicas;


import org.openstreetmap.gui.jmapviewer.Coordinate;






public class Cliente 
{

	
	transient private Coordinate ubicacion;
	private double Longitud;
	private double Latitud;
	
	public Cliente(Coordinate ubicacion)
	{
		this.ubicacion = ubicacion;
		this.setLatitud(ubicacion.getLat());
		this.setLongitud(ubicacion.getLon());
	}

	public double getLongitud() {
		return Longitud;
	}

	public void setLongitud(double longitud) {
		Longitud = longitud;
	}

	public double getLatitud() {
		return Latitud;
	}

	public void setLatitud(double latitud) {
		Latitud = latitud;
	}

	public Coordinate getUbicacion() {
		return ubicacion;
	}
	
	@Override
	public String toString()
	{
		return "Ubicacion: "+ this.ubicacion.getLat()+", "+this.ubicacion.getLon();
	}




}
