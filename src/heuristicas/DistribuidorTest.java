package heuristicas;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

class DistribuidorTest {

	@Test
	void igualesTest() 
	{
		Distribuidor i = new Distribuidor(new Coordinate(-34.52392,-58.70240));
		Distribuidor j= new Distribuidor(new Coordinate(-34.52392,-58.70240));
		
		assertTrue(i.iguales(j));
	}
	
	@Test
	void distintosTest() 
	{
		Distribuidor i = new Distribuidor(new Coordinate(-34.52392,-58.70240));
		Distribuidor j= new Distribuidor(new Coordinate(-34.52335,-58.70286));
		
		assertFalse(i.iguales(j));
	}

}
