package heuristicas;

import java.util.ArrayList;

public class Solucion 
{
	private ArrayList<Distribuidor> distribuidores;
	
	public Solucion()
	{
		distribuidores = new ArrayList<Distribuidor>();
	}
	
	public void agregar(Distribuidor distribuidor)
	{
		distribuidores.add(distribuidor);
	}
	
	public Distribuidor getDistribuidor(int i)
	{
		return this.distribuidores.get(i);
	}

	public ArrayList<Distribuidor> getDistribuidores() {
		return distribuidores;
	}


	public int cardinal()
	{
		return distribuidores.size();
	}
	
	 public double costoTotal(Instancia i)
	    {
		 	
		 	double distanciaMinimasTotales=0;
		 	for (Cliente cliente: i.getClientes())
				distanciaMinimasTotales+= distanciaMinima(cliente);
		 	
	    	return distanciaMinimasTotales;
			
	    }

	private double distanciaMinima(Cliente c) {
		
		 
		double distanciaMenor=0;
			for (int i = 0; i < this.distribuidores.size(); i++) 
				if( i==0 || distanciaMenor > this.distribuidores.get(i).calcularDistancia(c)) {
					distanciaMenor = this.distribuidores.get(i).calcularDistancia(c);
			}
			
			return distanciaMenor;
			
		
	}
	
}
