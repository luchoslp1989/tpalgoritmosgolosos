package heuristicas;


import java.util.ArrayList;

import org.openstreetmap.gui.jmapviewer.Coordinate;





public class Distribuidor 
{

	
	transient private double distanciaPromedio;
	transient private Coordinate ubicacion;
	private double Longitud;
	private double Latitud;
	
	public Distribuidor(Coordinate ubicacion)
	{
		this.ubicacion = ubicacion;
		this.setLatitud(ubicacion.getLat());
		this.setLongitud(ubicacion.getLon());
	}

	private void setLongitud(double lon) {

		this.Longitud = lon;
		
	}

	private void setLatitud(double lat) {

		this.Latitud = lat;
		
	}
	
	public double getLongitud() {
		return Longitud;
	}
	
	public double getLatitud() {
		return Latitud;
	}

	public Coordinate getUbicacion() {
		return ubicacion;
	}
	
	public void calcularDistanciaPromedio(ArrayList<Cliente> clientes)
	{
		double distanciaTotal=0;
		for (Cliente cliente : clientes) {
			distanciaTotal+= calcularDistancia(cliente);
			
		}
		
		this.distanciaPromedio = distanciaTotal / clientes.size();
	}
	
	public double calcularDistancia(Cliente cliente) {

			double RadioTierra = 6371; // km

			double lat1 = Math.toRadians(this.getUbicacion().getLat());
			double lon1 = Math.toRadians(this.getUbicacion().getLon());
			double lat2 = Math.toRadians(cliente.getUbicacion().getLat());
			double lon2 = Math.toRadians(cliente.getUbicacion().getLon());

			double diferenciaLong = (lon2 - lon1);
			double diferenciaLat = (lat2 - lat1);

			double sinlat = Math.sin(diferenciaLat / 2);
			double sinlon = Math.sin(diferenciaLong / 2);

			double a = (sinlat * sinlat) + Math.cos(lat1)*Math.cos(lat2)*(sinlon*sinlon);
			double c = 2 * Math.asin (Math.min(1.0, Math.sqrt(a)));

			double distancia = RadioTierra * c * 1000;

			return distancia;

			}
	
		public double distanciaPromedio()
		{
			return this.distanciaPromedio;
		}
		
		public boolean iguales(Distribuidor d) 
		{
			return this.Latitud == d.Latitud && this.Longitud == d.Longitud;
		}
	
	




}
