package heuristicas;

import java.util.ArrayList;

public class Instancia 
{
	private int cantidadMaximaPosible;
	private ArrayList<Distribuidor> distribuidores;
	private ArrayList<Cliente>  clientes;
	
	public Instancia(int k)
	{
		cantidadMaximaPosible = k;
		distribuidores = new ArrayList<Distribuidor>();
		clientes = new ArrayList<Cliente>();
		
		
		
	}
	
	public void agregarDistribuidor(Distribuidor d)
	{
		distribuidores.add(d);
	}
	
	public void agregarCliente(Cliente c)
	{
		clientes.add(c);
	}
	
	public int getTamano()
	{
		return distribuidores.size();
	}
	
	public int getCantidadDistribuidores()
	{
		return distribuidores.size();
	}
	
	
	
	public int getCantidadMaxima()
	{
		return cantidadMaximaPosible;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Distribuidor> getDistribuidores()
	{
		return (ArrayList<Distribuidor>) distribuidores.clone();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Cliente> getClientes()
	{
		return (ArrayList<Cliente>) clientes.clone();
	}

	public Distribuidor getObjeto(int i)
	{
		return distribuidores.get(i);
	}
	
	public Cliente getCliente(int i)
	{
		return clientes.get(i);
	}
	
	public void asignarDistancias() {
		for (Distribuidor d : distribuidores) {
			d.calcularDistanciaPromedio(clientes);
	}
}
}