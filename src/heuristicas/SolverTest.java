package heuristicas;


import static org.junit.jupiter.api.Assertions.*;

import java.text.DecimalFormat;

import org.junit.jupiter.api.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

class SolverTest {

	@Test
	public void cantidadMediaTest()
	{
		Solver solver = new Solver(ejemplo(), (uno, otro) -> {return uno.distanciaPromedio() < otro.distanciaPromedio() ? -1: uno.distanciaPromedio() == otro.distanciaPromedio()? 0: 1;});
		Solucion solucion = solver.resolver();
		
		Distribuidor i = new Distribuidor(new Coordinate(-34.52392,-58.7024));
		Distribuidor j= new Distribuidor(new Coordinate(-34.5219,-58.7059));
		
		
		assertTrue(i.iguales(solucion.getDistribuidor(0)));
		assertTrue(j.iguales(solucion.getDistribuidor(1)));
				
	}
	
	@Test
	public void cantidadCeroTest()
	{
		Solver solver = new Solver(ejemploCantidadCero(), (uno, otro) -> {return uno.distanciaPromedio() < otro.distanciaPromedio() ? -1: uno.distanciaPromedio() == otro.distanciaPromedio()? 0: 1;});
		Solucion solucion = solver.resolver();
		
		
		
		assertTrue(solucion.cardinal() == 0);
				
	}
	
	
	@Test
	public void cantidadExcedidaTest()
	{
		Solver solver = new Solver(ejemploExcedido(), (uno, otro) -> {return uno.distanciaPromedio() < otro.distanciaPromedio() ? -1: uno.distanciaPromedio() == otro.distanciaPromedio()? 0: 1;});
		Solucion solucion = solver.resolver();
		
		Distribuidor i = new Distribuidor(new Coordinate(-34.52392,-58.7024));
		Distribuidor j= new Distribuidor(new Coordinate(-34.5219,-58.7059));
		Distribuidor k= new Distribuidor(new Coordinate(-34.52000,-58.70100));
		Distribuidor l= new Distribuidor(new Coordinate(-34.52655,-58.70450));
		
		
		assertTrue(i.iguales(solucion.getDistribuidor(0)));
		assertTrue(j.iguales(solucion.getDistribuidor(1)));
		assertTrue(k.iguales(solucion.getDistribuidor(2)));
		assertTrue(l.iguales(solucion.getDistribuidor(3)));
				
	}
	
	@Test
	public void costoCantidadCeroTest()
	{
		Solver solver = new Solver(ejemploCantidadCero(), (uno, otro) -> {return uno.distanciaPromedio() < otro.distanciaPromedio() ? -1: uno.distanciaPromedio() == otro.distanciaPromedio()? 0: 1;});
		Solucion solucion = solver.resolver();
		
		
		
		assertTrue(solucion.costoTotal(ejemploCantidadCero()) == 0);
				
	}
	
	@Test
	public void costoTest()
	{
		Solver solver = new Solver(ejemplo(), (uno, otro) -> {return uno.distanciaPromedio() < otro.distanciaPromedio() ? -1: uno.distanciaPromedio() == otro.distanciaPromedio()? 0: 1;});
		Solucion solucion = solver.resolver();

		
	
		assertTrue(solucion.costoTotal(ejemplo()) == 1245.145114991944);
				
	}
	
	@Test
	public void costoInstanciaExcedidaTest()
	{
		Solver solver = new Solver(ejemploExcedido(), (uno, otro) -> {return uno.distanciaPromedio() < otro.distanciaPromedio() ? -1: uno.distanciaPromedio() == otro.distanciaPromedio()? 0: 1;});
		Solucion solucion = solver.resolver();
		
		
		
		assertTrue(solucion.costoTotal(ejemploExcedido()) == 943.7674063149473);
		
				
	}
	
	
	
	
	
	private Instancia ejemplo()
	{
		Instancia ret = new Instancia(2);
		Cliente a = new Cliente(new Coordinate(-34.52104,-58.70050));
		Cliente b= new Cliente(new Coordinate(-34.52210,-58.70700));
		Cliente c= new Cliente(new Coordinate(-34.52310,-58.70200));
		Cliente d= new Cliente(new Coordinate(-34.52404,-58.70550));
		Cliente e= new Cliente(new Coordinate(-34.52504,-58.70450));
		Cliente f= new Cliente(new Coordinate(-34.52204,-58.70250));
		
		ret.agregarCliente(a); ret.agregarCliente(b); ret.agregarCliente(c); ret.agregarCliente(d); ret.agregarCliente(e); ret.agregarCliente(f);
		
		Distribuidor i = new Distribuidor(new Coordinate(-34.52392,-58.70240));
		Distribuidor j= new Distribuidor(new Coordinate(-34.52190,-58.70590));
		Distribuidor k= new Distribuidor(new Coordinate(-34.52000,-58.70100));
		Distribuidor l= new Distribuidor(new Coordinate(-34.52655,-58.70450));
		
		ret.agregarDistribuidor(i); ret.agregarDistribuidor(j); ret.agregarDistribuidor(k); ret.agregarDistribuidor(l);

		ret.asignarDistancias();
		

	
		return ret;
	}
	
	private Instancia ejemploCantidadCero()
	{
		Instancia ret = new Instancia(0);
		Cliente a = new Cliente(new Coordinate(-34.52104,-58.70050));
		Cliente b= new Cliente(new Coordinate(-34.52210,-58.70700));
		Cliente c= new Cliente(new Coordinate(-34.52310,-58.70200));
		Cliente d= new Cliente(new Coordinate(-34.52404,-58.70550));
		Cliente e= new Cliente(new Coordinate(-34.52504,-58.70450));
		Cliente f= new Cliente(new Coordinate(-34.52204,-58.70250));
		
		ret.agregarCliente(a); ret.agregarCliente(b); ret.agregarCliente(c); ret.agregarCliente(d); ret.agregarCliente(e); ret.agregarCliente(f);
		
		Distribuidor i = new Distribuidor(new Coordinate(-34.52392,-58.70240));
		Distribuidor j= new Distribuidor(new Coordinate(-34.52190,-58.70590));
		Distribuidor k= new Distribuidor(new Coordinate(-34.52000,-58.70100));
		Distribuidor l= new Distribuidor(new Coordinate(-34.52655,-58.70450));
		
		ret.agregarDistribuidor(i); ret.agregarDistribuidor(j); ret.agregarDistribuidor(k); ret.agregarDistribuidor(l);

		ret.asignarDistancias();
		

	
		return ret;
	}
	
	private Instancia ejemploExcedido()
	{
		DecimalFormat formato = new DecimalFormat("#.00");
		Instancia ret = new Instancia(5);
		Cliente a = new Cliente(new Coordinate(-34.52104,-58.70050));
		Cliente b= new Cliente(new Coordinate(-34.52210,-58.70700));
		Cliente c= new Cliente(new Coordinate(-34.52310,-58.70200));
		Cliente d= new Cliente(new Coordinate(-34.52404,-58.70550));
		Cliente e= new Cliente(new Coordinate(-34.52504,-58.70450));
		Cliente f= new Cliente(new Coordinate(-34.52204,-58.70250));
		
		ret.agregarCliente(a); ret.agregarCliente(b); ret.agregarCliente(c); ret.agregarCliente(d); ret.agregarCliente(e); ret.agregarCliente(f);
		
		Distribuidor i = new Distribuidor(new Coordinate(-34.52392,-58.70240));
		Distribuidor j= new Distribuidor(new Coordinate(-34.52190,-58.70590));
		Distribuidor k= new Distribuidor(new Coordinate(-34.52000,-58.70100));
		Distribuidor l= new Distribuidor(new Coordinate(-34.52655,-58.70450));
		
		ret.agregarDistribuidor(i); ret.agregarDistribuidor(j); ret.agregarDistribuidor(k); ret.agregarDistribuidor(l);

		ret.asignarDistancias();

	
		return ret;
	}

}
