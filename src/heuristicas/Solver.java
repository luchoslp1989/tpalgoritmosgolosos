package heuristicas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Solver 
{
	private Instancia instancia;
	private Comparator<Distribuidor> comparador;
	
	public Solver(Instancia instancia, Comparator<Distribuidor> comparador)
	{
		this.instancia = instancia;
		this.comparador = comparador;
		
	}
	
	
	
	
	public Solucion resolver()
	{
		Solucion ret = new Solucion();
		for (Distribuidor distribuidor : distribuidoresEnOrden()) 
		{
			if( ret.cardinal() < instancia.getCantidadMaxima())
				ret.agregar(distribuidor);
		}
		
		return ret;
	}

	private ArrayList<Distribuidor> distribuidoresEnOrden() 
	{
		
		ArrayList<Distribuidor> ret = instancia.getDistribuidores();
		Collections.sort(ret, comparador);
		
		return ret;
	}
	
}
